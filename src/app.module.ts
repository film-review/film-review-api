/**
 * @format
 */

import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { typeOrmConfig } from './constant/typeorm.config';
import { UsersModule } from './users/users.module';
import { CategoriesModule } from './categories/categories.module';
import { FilmsModule } from './films/films.module';
import { ReviewsModule } from './reviews/reviews.module';
import { DirectorModule } from './director/director.module';
import { CastModule } from './cast/cast.module';
import { MulterModule } from '@nestjs/platform-express';
import { AuthModule } from './auth/auth.module';
import { AuthController } from './auth/auth.controller';

@Module({
  imports: [
    TypeOrmModule.forRoot(typeOrmConfig),
    CategoriesModule,
    CastModule,
    DirectorModule,
    FilmsModule,
    ReviewsModule,
    UsersModule,
    AuthModule,
    MulterModule.register({
      dest: './upload',
    }),
  ],
  controllers: [AppController, AuthController],
  providers: [AppService],
})
export class AppModule {}
