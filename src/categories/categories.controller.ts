/**
 * @format
 */

import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { CategoriesService } from './categories.service';
import { CategoriesEntity } from './categories.entity';
import { CategoriesDto } from './categories.dto';

@ApiTags('Categories')
@Controller('categories')
export class CategoriesController {
  constructor(private readonly categoriesService: CategoriesService) {}

  @Get()
  getAll(): Promise<CategoriesEntity[]> {
    return this.categoriesService.getAllCategories();
  }

  @Get(':category')
  getCategoryByName(@Param('category') category: string) {
    return this.categoriesService.getCategoryByName(category);
  }

  @Post()
  addCategory(@Body() data: CategoriesDto) {
    return this.categoriesService.addCategory(data);
  }

  @Put(':id')
  updateCategory(@Param('id') categoryId: number, @Body() data: CategoriesDto) {
    return this.categoriesService.editCategory(categoryId, data);
  }

  @Delete(':id')
  deleteCategory(@Param('id') categoryId: number) {
    return this.categoriesService.deleteCategory(categoryId);
  }
}
