/**
 * @format
 */

import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CategoriesEntity } from './categories.entity';
import { Repository } from 'typeorm';
import { CategoriesDto } from './categories.dto';

@Injectable()
export class CategoriesService {
  constructor(
    @InjectRepository(CategoriesEntity)
    private readonly categoriesRepository: Repository<CategoriesEntity>,
  ) {}

  async getAllCategories(): Promise<CategoriesEntity[]> {
    return await this.categoriesRepository.find();
  }

  async getCategoryByName(category: string) {
    return this.categoriesRepository.find({ where: { category } });
  }

  async addCategory(data: CategoriesDto) {
    try {
      return await this.categoriesRepository.save(data);
    } catch (e) {
      throw e;
    }
  }

  async editCategory(id: number, data) {
    try {
      return await this.categoriesRepository.update(id, data);
    } catch (e) {
      throw e;
    }
  }

  async deleteCategory(id: number) {
    try {
      return await this.categoriesRepository.delete({ id });
    } catch (e) {
      throw e;
    }
  }
}
