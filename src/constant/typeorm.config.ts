/**
 * @format
 */

import 'dotenv/config';

export const typeOrmConfig: Object = {
  type: 'mysql',
  host: process.env.DATABASE_HOST || 'localhost',
  port: Number(process.env.DATABASE_PORT) || 3306,
  username: process.env.DATABASE_USERNAME || 'root',
  password: process.env.DATABASE_PASSWORD || '12345678',
  database: process.env.DATABASE_DBNAME || 'film_reviews_database',
  entities: ['dist/**/*.entity{.ts,.js}'],
  synchronize: true,
  logging: true,
};
