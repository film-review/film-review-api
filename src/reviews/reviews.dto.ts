/**
 * @format
 */

import { ApiProperty } from '@nestjs/swagger';

export class ReviewsDto {
  @ApiProperty()
  content: string;
}
