/**
 * @format
 */

import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ReviewsEntity } from './reviews.entity';
import { Repository } from 'typeorm';

@Injectable()
export class ReviewsService {
  constructor(
    @InjectRepository(ReviewsEntity)
    private readonly reviewsRepository: Repository<ReviewsEntity>,
  ) {}

  async getAllReviews() {
    try {
      return await this.reviewsRepository.find();
    } catch (e) {
      throw e;
    }
  }

  async addReview(filmId: number, reviewsData) {
    try {
      const query = `INSERT INTO reviews(content, filmId) VALUES ('${
        reviewsData.content
      }', ${Number(filmId)})`;
      return await this.reviewsRepository.query(query);
    } catch (e) {
      throw e;
    }
  }
}
