/**
 * @format
 */

import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { ReviewsService } from './reviews.service';
import { ApiTags } from '@nestjs/swagger';
import { ReviewsDto } from './reviews.dto';

@ApiTags('Reviews')
@Controller('reviews')
export class ReviewsController {
  constructor(private readonly reviewsService: ReviewsService) {}

  @Get()
  getAllReviews() {
    return this.reviewsService.getAllReviews();
  }

  @Post(':filmId')
  addReview(@Param('filmId') filmId: number, @Body() content: ReviewsDto) {
    return this.reviewsService.addReview(filmId, content);
  }
}
