/**
 * @format
 */

import {
  Body,
  Controller,
  Get,
  Post,
  UseGuards,
  Request,
  Put,
  Param,
  Query,
} from '@nestjs/common';
import { ApiBearerAuth, ApiProperty, ApiTags } from '@nestjs/swagger';
import { LocalAuthGuard } from './local-auth.guard';
import { AuthService } from './auth.service';
import { JwtAuthGuard } from './jwt-auth.guard';
import { UsersService } from '../users/users.service';
import { UsersDto } from '../users/users.dto';
import { RegisterDto } from './auth.dto';

class testDto {
  @ApiProperty()
  username: string;

  @ApiProperty()
  password: string;
}

@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private readonly usersService: UsersService,
  ) {}

  @UseGuards(LocalAuthGuard)
  @Post('login-test')
  test(@Body() req: testDto) {
    return this.authService.login(req);
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Get('profile')
  getProfile(@Request() req) {
    return this.usersService.findUserByEmail(req.user.email);
  }

  @Post('register')
  register(@Body() registerData: RegisterDto) {
    return this.authService.register(registerData);
  }

  @ApiBearerAuth()
  @UseGuards(LocalAuthGuard)
  @Post('login')
  login(@Body() loginData: RegisterDto) {
    return this.authService.login(loginData);
  }

  @ApiBearerAuth()
  @Put('update-profile/:id')
  updateProfile(@Body() userData: UsersDto, @Param('id') id: number) {
    return this.usersService.updateProfile(Number(id), userData);
  }
}
