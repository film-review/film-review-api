/**
 * @format
 */

import { BadRequestException, Injectable } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import { UsersEntity } from '../users/users.entity';
import { Repository } from 'typeorm';
import { RegisterDto } from './auth.dto';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
    @InjectRepository(UsersEntity)
    private readonly usersRepository: Repository<UsersEntity>,
  ) {}

  async validateUser(email: string, password: string): Promise<any> {
    const user = await this.usersService.findUserByEmail(email);
    if (user && user.password === password) {
      const { password, email } = user;
      return { password, email };
    }
    return null;
  }

  async login(user: any) {
    const payload = { email: user.email, password: user.password };
    return {
      accessToken: this.jwtService.sign(payload),
    };
  }

  async register(registerData: RegisterDto) {
    try {
      const result = await this.usersRepository.findOne({
        where: { email: registerData.email },
      });
      if (!result) {
        return await this.usersRepository.save({
          email: registerData.email,
          password: registerData.password,
        });
      } else {
        return new BadRequestException('Email is already existed');
      }
    } catch (e) {
      throw e;
    }
  }
}
