/**
 * @format
 */

import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { FilmsEntity } from './films.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { FilmsDto } from './films.dto';
import { CategoriesEntity } from '../categories/categories.entity';
import { DirectorEntity } from '../director/director.entity';
import { CastEntity } from '../cast/cast.entity';

@Injectable()
export class FilmsService {
  constructor(
    @InjectRepository(FilmsEntity)
    private readonly filmRepository: Repository<FilmsEntity>,
    @InjectRepository(CategoriesEntity)
    private readonly categoryRepository: Repository<CategoriesEntity>,
    @InjectRepository(DirectorEntity)
    private readonly directorRepository: Repository<DirectorEntity>,
    @InjectRepository(CastEntity)
    private readonly castRepository: Repository<CastEntity>,
  ) {}

  async getListFilms() {
    return await this.filmRepository.find({
      relations: ['director', 'categories', 'reviews', 'cast'],
    });
  }

  async getFilmByName(name: string) {
    return await this.filmRepository.find({ where: { title: name } });
  }

  async getFilmById(id: number) {
    try {
      return await this.filmRepository.findOne({
        where: { id },
        relations: ['director', 'categories', 'reviews', 'cast'],
      });
    } catch (e) {
      throw e;
    }
  }

  async addFilm(filmData: FilmsDto) {
    try {
      const result = await Promise.all([
        await this.filmRepository.save({
          title: filmData.title,
          description: filmData.description,
          thumbnail: filmData.thumbnail,
          rating: filmData.rating,
          duration: filmData.duration,
          premiere: filmData.premiere,
        }),
        await this.directorRepository.save(filmData.directors),
        await this.categoryRepository.save(filmData.categories),
        await this.castRepository.save(filmData.casts),
      ]);
      const filmCategoriesQuery = `INSERT INTO films_categories_categories(filmsId, categoriesId) VALUES (${result[0].id}, ${result[2].id})`;
      await this.filmRepository.query(filmCategoriesQuery);
      const filmDirectorQuery = `INSERT INTO films_director_director(filmsId, directorId) VALUES (${result[0].id}, ${result[1].id})`;
      await this.filmRepository.query(filmDirectorQuery);
      return result;
    } catch (e) {
      throw e.message;
    }
  }

  async deleteFilById(filmId: number) {
    return await this.filmRepository.delete({ id: filmId });
  }

  async editFilmById(filmId: number, filmData: FilmsDto) {
    try {
      return await Promise.all([
        await this.filmRepository.update(filmId, {
          title: filmData.title,
          description: filmData.description,
          thumbnail: filmData.thumbnail,
          rating: filmData.rating,
          duration: filmData.duration,
          premiere: filmData.premiere,
        }),
        await this.directorRepository.update(filmId, filmData.directors),
        await this.categoryRepository.update(filmId, filmData.categories),
      ]);
    } catch (e) {
      throw e.message;
    }
  }
}
