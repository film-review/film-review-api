/**
 * @format
 */

import { ApiProperty } from '@nestjs/swagger';
import { CategoriesDto } from '../categories/categories.dto';
import { DirectorDto } from '../director/director.dto';
import { ReviewsDto } from '../reviews/reviews.dto';
import { CastDto } from '../cast/cast.dto';

export class FilmsDto {
  @ApiProperty()
  title: string;

  @ApiProperty()
  description: string;

  @ApiProperty()
  thumbnail: string;

  @ApiProperty()
  rating: number;

  @ApiProperty()
  duration: number;

  @ApiProperty()
  premiere: Date;

  @ApiProperty()
  directors: DirectorDto;

  @ApiProperty()
  categories: CategoriesDto;

  @ApiProperty()
  casts: CastDto[];
}
