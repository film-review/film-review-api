/**
 * @format
 */

import { Module } from '@nestjs/common';
import { FilmsController } from './films.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FilmsEntity } from './films.entity';
import { FilmsService } from './films.service';
import { CategoriesEntity } from '../categories/categories.entity';
import { DirectorEntity } from '../director/director.entity';
import { CastEntity } from '../cast/cast.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      FilmsEntity,
      CategoriesEntity,
      DirectorEntity,
      CastEntity,
    ]),
  ],
  controllers: [FilmsController],
  providers: [FilmsService],
})
export class FilmsModule {}
