/**
 * @format
 */

import {
  Controller,
  Get,
  Param,
  Post,
  Body,
  Delete,
  Put,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { FilmsService } from './films.service';
import { FilmsDto } from './films.dto';

@ApiTags('Films')
@Controller('films')
export class FilmsController {
  constructor(private readonly filmsService: FilmsService) {}

  @Get()
  getListFilms() {
    return this.filmsService.getListFilms();
  }

  @Get(':name')
  getFilmByName(@Param('name') name: string) {
    return this.filmsService.getFilmByName(name);
  }

  @Get(':id/detail')
  getFilmById(@Param('id') id: number) {
    return this.filmsService.getFilmById(id);
  }

  @Post()
  addFilm(@Body() filmData: FilmsDto) {
    return this.filmsService.addFilm(filmData);
  }

  @Delete(':filmId')
  deleteFilmById(@Param('filmId') filmId: number) {
    return this.filmsService.deleteFilById(filmId);
  }

  @Put(':filmId')
  editFilmById(@Param('filmId') filmId: number, @Body() filmData: FilmsDto) {
    return this.filmsService.editFilmById(filmId, filmData);
  }
}
