/**
 * @format
 */

import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { ReviewsEntity } from '../reviews/reviews.entity';
import { DirectorEntity } from '../director/director.entity';
import { CategoriesEntity } from '../categories/categories.entity';
import { CastEntity } from '../cast/cast.entity';

@Entity('films')
export class FilmsEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;

  @Column()
  description: string;

  @Column()
  thumbnail: string;

  @Column()
  rating: number;

  @Column()
  duration: number;

  @Column()
  premiere: Date;

  @ManyToMany(type => DirectorEntity, { cascade: true })
  @JoinTable()
  director: DirectorEntity[];

  @ManyToMany(type => CategoriesEntity, { cascade: true })
  @JoinTable()
  categories: CategoriesEntity[];

  @OneToMany(
    type => ReviewsEntity,
    reviews => reviews.film,
  )
  reviews: ReviewsEntity[];

  @OneToMany(
    type => CastEntity,
    cast => cast.film,
  )
  cast: CastEntity[];
}
