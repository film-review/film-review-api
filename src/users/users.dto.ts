/**
 * @format
 */

import { ApiProperty } from '@nestjs/swagger';

export class UsersDto {
  @ApiProperty()
  email: string;

  @ApiProperty()
  password: string;

  @ApiProperty()
  displayName: string;

  @ApiProperty()
  dateOfBirth: Date;

  @ApiProperty()
  fullName: string;
}
