/**
 * @format
 */

import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UsersEntity } from './users.entity';
import { UsersDto } from './users.dto';

@Injectable()
export class UsersService {
  private readonly users: Array<any>;

  constructor(
    @InjectRepository(UsersEntity)
    private readonly usersRepository: Repository<UsersEntity>,
  ) {}

  async getAllUser() {
    return await this.usersRepository.find();
  }

  async findUserByEmail(email: string) {
    const result = await this.usersRepository.findOne({ where: { email } });
    return {
      ...result,
    };
  }

  async updateProfile(id, userData: UsersDto) {
    if (userData.email && userData.password)
      throw new BadRequestException('Bạn không thể thay đổi email và mật khẩu');
    return await this.usersRepository.update(id, {
      ...userData,
      password: userData.password,
    });
  }
}
