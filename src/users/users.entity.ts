/**
 * @format
 */

import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('users')
export class UsersEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  email: string;

  @Column()
  password: string;

  @Column({ default: 'unknown' })
  displayName: string;

  @Column()
  dateOfBirth: Date;

  @Column({ default: 'unknown' })
  fullName: string;
}
