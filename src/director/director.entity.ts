/**
 * @format
 */

import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
const moment = require('moment');

@Entity('director')
export class DirectorEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ default: 'unknown' })
  fullName: string;

  @Column()
  dateOfBirth: Date;
}
