/**
 * @format
 */
import { ApiProperty } from '@nestjs/swagger';

export class DirectorDto {
  @ApiProperty()
  fullName: string;

  @ApiProperty()
  dateOfBirth: Date;
}
