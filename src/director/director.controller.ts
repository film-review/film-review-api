/**
 * @format
 */

import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { DirectorService } from './director.service';
import { ApiTags } from '@nestjs/swagger';
import { DirectorDto } from './director.dto';

@ApiTags('Director')
@Controller('director')
export class DirectorController {
  constructor(private readonly directorService: DirectorService) {}

  @Get()
  getAllDirectors() {
    return this.directorService.getAllDirectors();
  }

  @Get(':fullName')
  getDirectorByNamae(@Param('fullName') fullName: string) {
    return this.directorService.getDirectorByName(fullName);
  }

  @Delete(':id')
  deleteDirector(@Param('id') directorId: number) {
    return this.directorService.deleteDirector(directorId);
  }

  @Put(':id')
  updateDirector(@Param('id') directorId: number, @Body() data: DirectorDto) {
    return this.directorService.updateDirector(directorId, data);
  }

  @Post()
  addDirector(@Body() data: DirectorDto) {
    return this.directorService.addDirector(data);
  }
}
