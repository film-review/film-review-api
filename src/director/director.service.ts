/**
 * @format
 */

import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DirectorEntity } from './director.entity';
import { Repository } from 'typeorm';
import { DirectorDto } from './director.dto';

@Injectable()
export class DirectorService {
  constructor(
    @InjectRepository(DirectorEntity)
    private readonly directorRepository: Repository<DirectorEntity>,
  ) {}

  async getAllDirectors() {
    try {
      return await this.directorRepository.find();
    } catch (e) {
      throw e;
    }
  }

  async getDirectorByName(fullName: string) {
    try {
      return await this.directorRepository.find({ where: { fullName } });
    } catch (e) {
      throw e;
    }
  }

  async deleteDirector(directorId: number) {
    try {
      return await this.directorRepository.delete({ id: directorId });
    } catch (e) {
      throw e;
    }
  }

  async updateDirector(directorId: number, data: DirectorDto) {
    try {
      return await this.directorRepository.update(directorId, data);
    } catch (e) {
      throw e;
    }
  }

  async addDirector(data: DirectorDto) {
    try {
      return await this.directorRepository.save(data);
    } catch (e) {
      throw e;
    }
  }
}
