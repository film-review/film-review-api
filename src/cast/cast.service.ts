/**
 * @format
 */

import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CastEntity } from './cast.entity';

@Injectable()
export class CastService {
  constructor(
    @InjectRepository(CastEntity)
    private readonly castRepository: Repository<CastEntity>,
  ) {}
}
