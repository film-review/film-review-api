/**
 * @format
 */

import { Module } from '@nestjs/common';
import { CastService } from './cast.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CastEntity } from './cast.entity';

@Module({
  imports: [TypeOrmModule.forFeature([CastEntity])],
  providers: [CastService],
})
export class CastModule {}
