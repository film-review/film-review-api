/**
 * @format
 */

import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import { FilmsEntity } from '../films/films.entity';

@Entity('cast')
export class CastEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ default: 'unknown' })
  fullName: string;

  @ManyToOne(
    type => FilmsEntity,
    films => films.cast,
  )
  film: FilmsEntity;
}
