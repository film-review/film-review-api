/**
 * @format
 */

import { ApiProperty } from '@nestjs/swagger';

export class CastDto {
  @ApiProperty()
  fullName: string;
}
